resource "aws_launch_configuration" "LC" {

  name = "tf-launch-cfg"
  image_id = "${var.image_id}"
  instance_type = "${var.instance_type}"
  user_data = "${data.template_file.user_data.rendered}"
  security_groups = ["${var.SG-http}"]

  iam_instance_profile = "${aws_iam_instance_profile.profile.name}"

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "ASG" {

  name                 = "tf-ASG"
  launch_configuration = "${aws_launch_configuration.LC.id}"
  min_size             = "${var.min_size}"
  max_size             = "${var.max_size}"


  vpc_zone_identifier = ["${var.subnets_id}"]


  tag {
    key = "Name"
    value = "tf-${var.env}"
    propagate_at_launch = true
  }
}

resource "aws_iam_instance_profile" "profile" {
  name = "tf-profile"
  role = "${aws_iam_role.role.name}"

}

resource "aws_iam_role" "role" {
  name = "tf-cluster-role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "S3access" {
    name = "tf-s3-policy"
    path = "/"
    description = "Access to the bucket from nginx instances"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "S3:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "S3" {
    name       = "tf-s3-attachment"
    roles      = ["${aws_iam_role.role.name}"]
    policy_arn = "${aws_iam_policy.S3access.arn}"
}


data "template_file" "user_data" {
  template = "${file("${path.module}/user-data.sh")}"
}