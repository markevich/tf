variable "env" {
  description = "The env type (prod/stage/etc)"
  default = "dev"
}

variable "region" {
  description = "The aws region (eu-central-1)"
  default = "us-west-2"
}