#optional s3 bucket for remote state file
provider "aws" {
  region = "${var.region}"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "TFstate-${var.env}-envrandomstring"

  versioning {
    enabled = true
  }
}