data "aws_availability_zones" "available" {}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "tf-${var.env}-vpc"
  }
}

resource "aws_subnet" "subnets" {

  vpc_id = "${aws_vpc.vpc.id}"
  count = 3
  cidr_block = "${cidrsubnet("10.0.0.0/22", 2 , count.index )}"

  availability_zone = "${element(data.aws_availability_zones.available.names, count.index)}"
  tags {
    Name = "tf-${var.env}-subnet-public"
  }

  map_public_ip_on_launch = true
}


resource "aws_route_table" "route_table_public" {

  vpc_id = "${aws_vpc.vpc.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags {
    Name = "tf-${var.env}-RT-public"
  }
}

resource "aws_route_table_association" "rta" {
  count = 3
  route_table_id = "${aws_route_table.route_table_public.id}"
  subnet_id = "${element(aws_subnet.subnets.*.id, count.index )}"
}
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "tf-${var.env}-igw"
  }
}

resource "aws_security_group" "allow_http" {

  vpc_id = "${aws_vpc.vpc.id}"
  name = "allow_http"
  description = "A rule for HTTP traffic"

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags {
    Name = "tf-${var.env}-sg"
  }
}