provider "aws" {
  region = "us-west-2"
}

module "vpc" {
  source = "../../modules/vpc/"

  env = "${var.env}"
}

module "cluster" {
  source = "../../modules/cluster"

  image_id = "ami-e251209a"
  instance_type = "t2.micro"

  SG-http = "${module.vpc.SG-http}"
  subnets_id = ["${module.vpc.subnets_id}"]

  min_size = "3"
  max_size = "3"

  env = "${var.env}"
}

resource "aws_s3_bucket" "private_bucket" {
  bucket = "privates3bucketrandomstring"

  versioning {
    enabled = true
  }
}