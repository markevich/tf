output "subnets_id" {
  value = ["${aws_subnet.subnets.*.id}"]
}

output "SG-http" {
  value = "${aws_security_group.allow_http.id}"
}