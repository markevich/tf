variable "image_id" {
  description = "AMI id"
  default = "ami-e251209a"
}
variable "env" {
  description = "env type"
  default = "dev"
}
variable "instance_type" {
  description = "Type of the instances in cluster"
  default = "t2.micro"
}

variable "min_size" {
  description = "min size of cluster instances"
  default = 1
}

variable "max_size" {
  description = "max size of cluster instances"
  default = 3
}

variable "subnets_id" {
  type = "list"
  description = "Subnets ID"
}

variable "SG-http" {
  description = "Security group for HTTP"
}