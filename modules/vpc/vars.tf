variable "env" {
  description = "The env type (prod/stage/etc)"
  default = "dev"
}
